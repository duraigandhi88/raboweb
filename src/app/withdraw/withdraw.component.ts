import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/userservice.service';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {


    withDrawForm: FormGroup;
    validateError = "";
    successMsg="";
    apiResult: any;
  
    constructor(private route: Router,private userService: UserService) { }
  
    ngOnInit() {
      this.withDrawForm = new FormGroup({
        'description': new FormControl('', [Validators.required,Validators.pattern('^[a-zA-Z]+[a-zA-Z0-9\\s.,()@#$!]{2,50}$')]),
        'transactionAmount': new FormControl('', [Validators.required,Validators.pattern('^[1-9]+[0-9.]+$')]),
        'transactionType': new FormControl('withdraw')
      });
    }
  
    get description() { return this.withDrawForm.get("description"); }
    get transactionAmount() { return this.withDrawForm.get("transactionAmount"); }
    get transactionType() { return this.withDrawForm.get("transactionType"); }
  
    onSubmit() {
      this.userService.transaction(this.withDrawForm.value,this.userService.accountNumber).subscribe(
        data=>{
          this.apiResult = data;
       this.validateError="";
       if(this.apiResult.statusCode==200){
         this.refreshData();
        this.withDrawForm.reset();
         this.successMsg="successfully withdrawn "+this.apiResult.statusMessage;
       }
       else{
        this.validateError=this.apiResult.statusMessage;
        this.successMsg="";
       }
  
      },
      error =>{
        this.successMsg="";
        this.validateError="Service unavailable";
        console.log('oops', error);} 
    )
     
    }

    refreshData(){
      this.userService.getTransaction(this.userService.accountNumber).subscribe(
        data=>{
       this.apiResult = data;
       if(this.apiResult.statusCode==200){
         return this.apiResult.data;
       }
  
  })
  
}

}