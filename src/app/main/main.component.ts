import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
@ViewChild("myButton",{static: false}) myButton;

  constructor() { }

  ngOnInit() {
  }

  buttonMessage(){
    this.myButton.loadData();
  }

}
