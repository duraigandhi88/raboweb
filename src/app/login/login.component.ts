import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { error } from 'util';
import { UserService } from 'src/app/userservice.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  validateError = "";
  apiResult: any;

  constructor(private route: Router,private userService: UserService) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'userName': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required])
    });
  }

  get userName() { return this.loginForm.get("userName"); }

  get password() { return this.loginForm.get("password"); }

  onSubmit() {
    this.userService.signIn(this.loginForm.value).subscribe(
      data=>{
        this.validateError="";
     this.apiResult = data;
    
     if(this.apiResult.statusCode==200){
       this.userService.apiTransactionResult=this.apiResult.data;
       this.userService.accountNumber=this.userService.apiTransactionResult.accountNumber;
       this.userService.isLogined=true;
       this.route.navigate(["main"]);
     }
     else{
      this.validateError="Please provide correct credentials";
     }

    },
    error =>{
      this.validateError="Service unavailable";
      console.log('oops', error);} 
  )

  }

  signUp() {
    this.route.navigate(['signUp']);
  }
  
  }
