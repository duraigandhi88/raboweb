import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/userservice.service';
import { AppConstants } from '../app-constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  apiResult: any;
  transactionData: any;
  accountNumber: string ="";
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData(){
    this.userService.getTransaction(this.userService.accountNumber).subscribe(
      data=>{
     this.apiResult = data;
     if(this.apiResult.statusCode==200){
      this.accountNumber=this.apiResult.data.accountNumber;
       this.transactionData=this.apiResult.data.liTransactions;
     }
    }) 
  }

  

  downloadCSV(){
    window.open(AppConstants.baseURL+'download/'+this.accountNumber+'/transaction.csv');
  }

  downloadXML(){
    window.open(AppConstants.baseURL+'download/'+this.accountNumber+'/transaction.xml');
  }

}
