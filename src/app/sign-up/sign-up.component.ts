import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { UserService } from 'src/app/userservice.service';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  validateError = "";
  successMsg="";
  apiResult: any;

  constructor(private route: Router,private userService: UserService) { }

  ngOnInit() {
    this.signUpForm = new FormGroup({
      'firstName': new FormControl('', [Validators.required,Validators.pattern('^[a-zA-Z]+[a-zA-Z0-9\\s.]{2,50}$')]),
      'lastName': new FormControl('',Validators.pattern('^[a-zA-Z]+[a-zA-Z0-9\\s.]{2,50}$')),
      'userName': new FormControl('', [Validators.required,Validators.pattern('^[a-zA-Z]+[a-zA-Z0-9\\s.]{2,50}$')]),
      'password': new FormControl('', [Validators.required,Validators.pattern('^[a-zA-Z0-9@#!^*|?<>,()&%-_\\s.]{6,12}$')]),
      'email': new FormControl('', [Validators.required,Validators.email]),
      'phone': new FormControl('', [Validators.required,Validators.pattern('^[0-9+\\s]{10,13}$')]),
      'startBalance': new FormControl('', [Validators.required,Validators.pattern('^[1-9]+[0-9.]+$')]),
    });
  }

  get firstName() { return this.signUpForm.get("firstName"); }

  get lastName() { return this.signUpForm.get("lastName"); }

  get userName() { return this.signUpForm.get("userName"); }

  get email() { return this.signUpForm.get("email"); }


  get phone() { return this.signUpForm.get("phone"); }

  get startBalance() { return this.signUpForm.get("startBalance");}

  get password() { return this.signUpForm.get("password"); }


  onSubmit() {
    this.userService.createAccount(this.signUpForm.value).subscribe(
       data=>{
         this.apiResult = data;
      this.validateError="";
      console.log(this.apiResult);
      if(this.apiResult.statusCode==200){
       this.signUpForm.reset();
        this.successMsg="Account successfully created "+this.apiResult.statusMessage;
      }
      else{
       this.validateError=this.apiResult.statusMessage;
       this.successMsg="";
      }

     },
     error =>{
       this.successMsg="";
       this.validateError="Service unavailable";
       console.log('oops', error);} 
   );
   
  }

  signIn() {
    this.route.navigate(['']);
  }

}
