import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { MainComponent } from 'src/app/main/main.component';
import { AuthGuard } from 'src/app/auth.guard';

const routes: Routes = [{path:'',component:LoginComponent},
{path:'signUp',component:SignUpComponent },
{path:'main',component:MainComponent,canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
