import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders } from '@angular/common/http';
import { AppConstants } from 'src/app/app-constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isLogined: boolean=false;
  accountNumber: string="";
  apiTransactionResult: any;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })};


  constructor(private http: HttpClient) {
    
   }

   signIn(body) {
    console.log(body);
    return this.http.post(AppConstants.baseURL + "login", body, this.httpOptions)
  }

  createAccount(body) {
    console.log(body);
    return this.http.post(AppConstants.baseURL + "createAccount", body, this.httpOptions)
  }

  transaction(body,accountNumber) {
    console.log(body);
    return this.http.post(AppConstants.baseURL + "transaction/"+accountNumber, body, this.httpOptions)
  }

  getTransaction(accountNumber) {
    return this.http.get(AppConstants.baseURL + "transaction/"+accountNumber, this.httpOptions)
  }



  
  
}
