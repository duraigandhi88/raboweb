import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './userservice.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  

  constructor(private userService: UserService,private router: Router){

  }
  canActivate(){
    if(this.userService.isLogined){
      return true;
    }
    else {
      window.alert("You don't have permission to login first"); 
      this.router.navigate([""]);
      return false;
    }
  }
}

  

